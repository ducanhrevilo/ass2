var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var app = express();
var viewsPath = __dirname + '/views/';
var index = require('./routes/index');
var server = require('http').Server(app);
var io = require('socket.io')(server);

//Using ejs
app.set('view engine', 'ejs');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use('/vendor', express.static(path.join(__dirname, 'vendor')));
app.use('/js', express.static(path.join(__dirname, 'js'))); 
app.use('/css', express.static(path.join(__dirname, 'css'))); 

app.use('/',index); 

app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

app.use(function(err, req, res, next) {
    // Set Locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // Render the error page
    res.status(err.status || 500);
    res.sendFile(viewsPath + "404.html");
});

require('./sockets')(io);

module.exports = {app: app, server: server};